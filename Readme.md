## plugin previewer

A plugin which enables preview of injected content and provides a content injection api.

### install

```node
npm install --save superewald/vuepress-plugin-previewer
```

### usage

