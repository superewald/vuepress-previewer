import PreviewContent from './components/PreviewContent'
import PreviewStorage from './services/PreviewStorage'
import EditorAPI from './services/EditorAPI'

export default ({Vue, opts, router, siteData, isServer}) => {
    if(!isServer) {
        const editApi = new EditorAPI()
        editApi.shakeHands()
        console.log('ye')
        Vue.prototype.editApi = editApi
        Vue.prototype.prevStorage = new PreviewStorage()
        router.beforeEach((to, from, next) => {
            if('preview' in to.query) {
                delete Vue.options.components['Content']
                Vue.component('Content', PreviewContent)
            }
            next()
        })
    }
}