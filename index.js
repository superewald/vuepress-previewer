const path = require('path')

module.exports = (options, context) => ({
    name: 'vuepress-plugin-previewer',
    enhanceAppFiles: path.resolve(__dirname, 'enhanceAppFiles.js'),
    chainWebpack(config) {
        // TODO: is this really required to have the dependencies bundled with webpack ?!
        config.resolve.modules.store.add(path.resolve(__dirname, 'node_modules'))
    }
})