import Postmate from 'postmate'
import PreviewStorage from './PreviewStorage'

class EditorAPI {
    constructor(opts) {
        this.options = {...{
            storage: null
        }, ...opts}
        this.storage = this.options.storage ?? new PreviewStorage()
        this.parent = null
        this.initialized = false
    }

    shakeHands() {
        const editApi = this
        this.handshake = new Postmate.Model({
            window: () => ({
                height: document.height || document.body.offsetHeight,
                width: document.width || document.body.offsetWidth
            }),
            // update the preview storage to add or update files
            async updatePreviewStorage(file) {
                // support receiving multiple files at once
                /*data.forEach(file => {
                    // if markdown file, compile to html and store
                    if(file.type === 'markdown') {
                        for(slotKey in file.data) {
                            editApi.storage.compileAndSave(file.path, file.data[slotKey], slotKey)
                        }
                    } 
                    // any other data is stored without intervention
                    else {
                        editApi.storage.save(file.path, file.data)
                    }
                })*/
                file.path = file.path.toLowerCase()
                console.log(`saving ${file.path}`)
                await editApi.storage.save(file.path, file.data)
            }
        })

        console.log(this.handshake)

        this.handshake.then(parent => {
            this.parent = parent
            this.initialized = true
            parent.emit('initialized')
        })
    }
}

export default EditorAPI