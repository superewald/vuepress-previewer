if(typeof window !== "undefined")
    window.global = window

const LFS = require('@isomorphic-git/lightning-fs')

class PreviewStorage {
    constructor(opts) {
        this.options = {...{
            branch: 'master',
            fs: null,
        }, ...opts}
        this.branch = this.options.branch
        this.fs = this.options.fs ?? new LFS(`vuepress-preview-${this.options.branch}`)
        this.pfs = this.fs.promises
    }

    
    read(path, cb) { 
        this.fs.readFile(path, 'utf8', cb)
    }

    read(path) {
        return this.pfs.readFile(path)
    }

    save(path, data, cb) {
        this.fs.writeFile(path, data, 'utf8', cb)
    }

    async exists(path) {
        try {
            let stat = await this.pfs.stat(path)
            return true
        } catch {
            return false
        }
    }

    async save(path, data) {
        try {
            return await this.pfs.writeFile(path, data)
        } catch(err) {
            try {
                if(!this.exists(dir)) {
                    let pathArr = path.split('/')
                    let filename = pathArr.pop()
                    let curr = '/'
                    pathArr.forEach(async (dir) => {
                        curr += dir + '/'
                        await this.pfs.mkdir(curr)
                    })
                }
            } catch(err) {
                console.log(err)
            }
        }
    }

    async getContent(path, slotKey = 'default') {
        path = path.toLowerCase()
        try {
            let raw = await this.read(path)
        } catch(err) {
            if('code' in err && err.code == 'ENOENT') {
                throw "Preview was not found for " + path
            }
            throw err
        }
        return JSON.parse(raw)[slotKey]
    }

    async saveContent(path, data, slotKey = 'default') {
        let oldData = null
        try {
            oldData = await this.read(path)
        } catch(err) {
            if('code' in err && err.code == 'ENOENT') {
            }
            throw err
        }
        oldData[slotKey] = data
        return this.save(path, oldData)
    }

    compileAndSave(path, data, slotKey = 'default') {

    }
}

export default PreviewStorage